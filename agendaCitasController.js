import AgendaCita from "../models/AgendaCita.js";

const agregar = async (req, res) => {

    try {
        const agendaCita = new AgendaCita(req.body);
        const agendaCitaGuardado = await agendaCita.save();
        res.json({ body: agendaCitaGuardado, ok: "SI" })
    } catch (error) {
        console.log(error);
    }
}

const listar = async (req, res) => {
    const agendaCitas = await AgendaCita.find();
    res.json(agendaCitas);
}

const eliminar = async (req, res) => {
    //recibir los parametros por la url
    const { id } = req.params;

    //validamos si existe el registro a eliminar
    const agendaCita = await AgendaCita.findById(id);

    if (!agendaCita) {
        const error = new Error("Registro no encontrado.");
        return res.status(404).json({ msg: error.message, ok: "NO" });
    }

    try {
        await agendaCita.deleteOne();
        res.json({ msg: "Registro eliminado correctamente.", ok: "SI" });
    } catch (error) {
        console.log(error);
    }
}

const editar = async (req, res) => {
    //recibir los parametros por la url
    const { id } = req.params;

    //validamos si existe el registro a eliminar
    const agendaCita = await AgendaCita.findById(id);

    if (!agendaCita) {
        const error = new Error("Registro no encontrado.");
        return res.status(404).json({ msg: error.message, ok: "NO" });
    }

    //capturar los datos enviados en el formulario
    agendaCita.idMedico = req.body.idMedico || agendaCita.idMedico;
    agendaCita.fechaCita = req.body.fechaCita || agendaCita.fechaCita;
    agendaCita.horaCita = req.body.horaCita || agendaCita.horaCita;
    agendaCita.numeroConsultorio = req.body.numeroConsultorio || agendaCita.numeroConsultorio;
    agendaCita.estadoCita = req.body.estadoCita || agendaCita.estadoCita;
    
    try {
        const agendaCitaGuardado = await agendaCita.save();
        res.json({ body: agendaCitaGuardado, msg: "Registro actualizado correctamente.", ok: "SI" });
    } catch (error) {
        console.log(error);
    }
}

const listarUno = async (req, res) => {
    //recibir los parametros por la url
    const { id } = req.params;

    //validamos si existe el registro a eliminar
    const agendaCita = await AgendaCita.findById(id);

    if (!agendaCita) {
        const error = new Error("Registro no encontrado.");
        return res.status(404).json({ msg: error.message, ok: "NO" });
    }

    res.json(agendaCita);
}

export {
    agregar,
    listar,
    eliminar,
    editar,
    listarUno
}