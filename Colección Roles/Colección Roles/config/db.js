import mongoose, { connect } from "mongoose";

const conectarDB = () => {
    const urlConexion = String(process.env.MONGO_URI);
    connect(urlConexion)
        .then(con => {
            console.log(`Conexion establecida con la base: ${urlConexion}`);
        })
        .catch(error => {
            console.log(error);
        });

    /*try {
        const connection = await mongoose.connect(
            process.env.MONGO_URI, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        }
        );

        const url = `${connection.connection.host}:${connection.connection.port}`;
        console.log(`MongoDB Conectado en ${url}`);
    } catch (error) {
        console.log(`Error: ${error.message}`);
        process.exit(1);
    }*/
};

export default conectarDB;